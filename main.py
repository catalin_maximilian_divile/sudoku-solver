
matrice = [[0,0,4,8,6,0,0,3,0],
           [0,0,1,0,0,0,0,9,0],
           [8,0,0,0,0,9,0,6,0],
           [5,0,0,2,0,6,0,0,1],
           [0,2,7,0,0,1,0,0,0],
           [0,0,0,0,4,3,0,0,6],
           [0,5,0,0,0,0,0,0,0],
           [0,0,9,0,0,0,4,0,0],
           [0,0,0,4,0,0,0,1,5]]
print(matrice)
import numpy as np
print(np.matrix(matrice))
def pozitie_valida(y, x, n):
    global matrice
    for i in range(0,9):
        if matrice[y][i]==n:
            return False
    for i in range(0,9):
        if matrice[i][x]==n:
            return False
    x0=(x//3)*3
    y0=(y//3)*3
    for i in range(0,3):
        for(j) in range (0,3):
            if matrice[y0 + i][x0 + j]==n:
                return False
    return True

def rezolvare():
    global matrice
    for y in range(9):
        for x in range(9):
            if matrice[y][x]==0:
                for n in range(1,10):
                    if pozitie_valida(y, x, n):
                        matrice[y][x]=n
                        rezolvare()
                        matrice[y][x]=0
                return
    print(np.matrix(matrice))
if __name__ =='__main__':
    rezolvare()